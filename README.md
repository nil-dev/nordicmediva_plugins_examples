# Running python examples
1. Open the Python folder in VSCode. *Important:* The Python folder must be the root folder in VSCode.
2. Open using devcontainer. See [wiki.nordicmediva.com](http://wiki.nordicmediva.com/developer/Python) for more info.

# Running C++ examples
1. Open the C++ folder in VSCode. *Important:* The C++ folder must be the root folder in VSCode.
2. Open using devcontainer. See [wiki.nordicmediva.com](http://wiki.nordicmediva.com/developer/C++) for more info.

