# For developing nordicMEDiVA using a devcontainer in VS Code

## Prerequisites
You must have the following installed on your native system:
* Docker
* Visual Studio Code

Further, you must install the following packackes in VS Code
* Remote - Containers (ms-vscode-remote.remote-containers)

## Setting CMake settings
To, for instance, tell CMake to build tests, or setting any CMake options created using the option() function, 
edit your .vscode/settings.json file (it is ignored by git). Add the following:
```
"cmake.configureArgs": [
    "-DCMAKE_TEST_DSC=ON"
] 
```
or edit it with any other CMake option you wish to set.

