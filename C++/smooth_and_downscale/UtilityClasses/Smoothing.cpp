#ifdef _WIN32
#define _USE_MATH_DEFINES
#endif
#include <math.h>
#include "Smoothing.h"

///
/// \brief Constructor.
///
Smoothing::Smoothing()
{

}

///
/// \brief Calculate gaussian kernel.
/// \param range [in] Range for the kernel.
/// \param stdDeviation [in] Standarf deviation to apply to the kernel.
/// \return Pointer to gaussian kernel.
///
std::shared_ptr<std::vector<float>> Smoothing::calcGaussianKernel(int range, double stdDeviation)
{
    auto k = std::make_shared<std::vector<float>>(2*range + 1);
    double sigma2 = stdDeviation*stdDeviation;
    double expPref = -1.0 / (2.0*sigma2);
    double pref = 1.0 / sqrt(2.0*M_PI*sigma2);
    for(int i=0; i<=(2*range); i++)
    {
        double x = static_cast<double>(i - range);
        (*k)[static_cast<size_t>(i)] = static_cast<float>( pref * exp(expPref * x*x) );
    }

    return k;
}
