#pragma once
#include <memory>
#include <Series/Series.h>

///
/// \ingroup SmoothAndDownscale_UtilityClasses
/// \brief Contains methods for calcularing various properties based on volume input, as well as preparing the volumes themselves.
///
class VolOperations
{
public:
    VolOperations();

public:
    static std::shared_ptr<Volume> createEmptyVolume(int numSlices, int numRows, int numColumns);
    static bool getVolumeSizes(const Volume& inputVolume, int& numSlices, int& numColumns, int& numRows);
    static std::shared_ptr<Volume> downsample(const Volume& inputVolume, int domainEdgeLengthInSlice, int domainHeight);
    static void copyDicomHeaders(const Volume& from, Volume& to);
};
