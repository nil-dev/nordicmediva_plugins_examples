#include "Plots.h"

///
/// \brief Constructor.
///
Plots::Plots()
{

}

///
/// \brief Generate a time curve, based on the middle slices of the given series.
/// \param series [in] Series to calculate the time curve for.
/// \return Points of time curve, given as pairs.
///
std::vector<std::pair<float,float>> Plots::genTimeCurve(const Series& series)
{
    std::vector<std::pair<float,float>> timeCurve(series.getVolumeCount());
    for(size_t v=0; v<series.getVolumeCount(); v++)
    {
        float sum = 0.0f;
        size_t k = series[v].getSliceCount() / 2;
        for(size_t j=0; j<series[v][k].getRowCount(); j++)
        {
            for(size_t i=0; i<series[v][k].getColumnCount(); i++)
            {
                sum+= series[v][k][j][i];
            }
        }

        timeCurve[v] = std::pair<float,float>(static_cast<float>(v), sum);
    }

    return timeCurve;
}
