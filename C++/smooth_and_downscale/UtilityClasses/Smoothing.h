#pragma once

#include <string>
#include <vector>
#include <memory>

///
/// \ingroup SmoothAndDownscale_UtilityClasses
/// \brief Contains utility functions used in the calculation of smoothing.
///
class Smoothing
{
public:
    Smoothing();

public:
    static std::shared_ptr<std::vector<float>> calcGaussianKernel(int range, double stdDeviation);
};
