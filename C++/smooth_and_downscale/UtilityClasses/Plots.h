#pragma once
#include <Series/Series.h>

///
/// \ingroup SmoothAndDownscale_UtilityClasses
/// \brief Contains static functions for generating various plots.
///
class Plots
{
public:
    Plots();

public:
    static std::vector<std::pair<float,float>> genTimeCurve(const Series& series);
};
