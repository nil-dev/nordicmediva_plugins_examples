#include <SeriesCreator/CreateSeries.h>
#include "VolOperations.h"

///
/// \brief Create an empty volume.
/// \param numSlices [in] Number of slices in new volume.
/// \param numRows [in] Number of rows in new volume.
/// \param numColumns [in] Number of columns in new volume.
/// \return Pointer to new volume.
///
std::shared_ptr<Volume> VolOperations::createEmptyVolume(int numSlices, int numRows, int numColumns)
{
    std::shared_ptr<Volume> emptyVolume = SeriesCreator::createEmptySeries(1, static_cast<uint16_t>(numSlices),
                                                                              static_cast<uint16_t>(numRows),
                                                                              static_cast<uint16_t>(numColumns))->getVolumes()[0];

    return emptyVolume;
}

///
/// \brief Return volume sizes.
/// \param inputVolume [in] Volume to find sizes of.
/// \param numSlices [out] Recieves the number of slices in inputVolume.
/// \param numColumns [out] Recieves the number of columns in inputVolume.
/// \param numRows [out] Recieves the number of rows in inputVolume.
/// \return Returns true is OK, else returns false.
///
bool VolOperations::getVolumeSizes(const Volume& inputVolume, int& numSlices, int& numColumns, int& numRows)
{
    numSlices = static_cast<int>(inputVolume.getSliceCount());
    if(numSlices < 1) return false;
    numColumns = static_cast<int>(inputVolume.getColumnCount());
    numRows = static_cast<int>(inputVolume.getRowCount());

    return true;
}

///
/// \brief A new volume is generated, which is a copy of the input volume, but with reduced resolution.
/// \param inputVolume [in] Input volume.
/// \param domainEdgeLengthInSlice [in] The number of rows / columns to combine into a single voxel.
/// \param domainHeight [in] The number of slices to combine into a single voxel.
/// \return Downsampled volume.
///
/// The downsample algorithm is as follows:
/// * Iterate through all voxels, \f$V_{i,j,k}\f$, within the downsampled volume
/// * For each such voxel, the input voxel domain
///   \f[\mathcal{D}_{i,j,k} = [i\cdot L_d, i\cdot L_d + L_d] \times [i\cdot L_d, i\cdot L_d + L_d] \times [i\cdot H, i\cdot H + H] \in \mathbb{N} \times \mathbb{N} \times \mathbb{N}\f]
///   of the input volume, \f$U\f$, where \f$L_d\f$ is the domain edge length and \f$H\f$ is the domain height, is averaged over, such that
///   \f[V_{i,j,k} = \frac{1}{N_{i,j,k}} \sum_{(I,J,K) \in \mathcal{D}_{i,j,k}} U_{I,J,K},\f]
///   where \f$N_{i,j,k}\f$ is the number of elements in \f$\mathcal{D}_{i,j,k}\f$. If \f$N_{i,j,k} = 0\f$ then \f$V_{i,j,k}\equiv 0\f$. Moreover, if \f$I, J\f$ or \f$K\f$ exceeds the
///   number of columns, rows or slices, respectively, then the corresponding entries are removed from the domain, \f$\mathcal{D}_{i,j,k}\f$.
///
std::shared_ptr<Volume> VolOperations::downsample(const Volume& inputVolume, int domainEdgeLengthInSlice, int domainHeight)
{
    // Create new, smaller, volume
    int numSlices, numRows, numColumns;
    if(!getVolumeSizes(inputVolume, numSlices, numColumns, numRows)) return nullptr;

    int numSlicesDownsampled = static_cast<int>(std::ceil(static_cast<double>(numSlices) / static_cast<double>(domainHeight)));
    int numRowsDownsampled = static_cast<int>(std::ceil(static_cast<double>(numRows) / static_cast<double>(domainEdgeLengthInSlice)));
    int numColumnsDownsampled = static_cast<int>(std::ceil(static_cast<double>(numColumns) / static_cast<double>(domainEdgeLengthInSlice)));
    std::shared_ptr<Volume> downsampledVolume = VolOperations::createEmptyVolume(numSlicesDownsampled, numRowsDownsampled, numColumnsDownsampled);

    // Average over cuboid domains in the original volume
    for(size_t k=0; k<static_cast<size_t>(numSlicesDownsampled); k++)
    {
        Slice& sliceDownsampled = (*downsampledVolume)[k];

        size_t domainStartK = k*static_cast<size_t>(domainHeight);
        size_t domainEndK = domainStartK + static_cast<size_t>(domainHeight);
        for(size_t i=0; i<static_cast<size_t>(numColumnsDownsampled); i++)
        {
            size_t domainStartI = i*static_cast<size_t>(domainEdgeLengthInSlice);
            size_t domainEndI = domainStartI + static_cast<size_t>(domainEdgeLengthInSlice);
            for(size_t j=0; j<static_cast<size_t>(numRowsDownsampled); j++)
            {
                size_t domainStartJ = j*static_cast<size_t>(domainEdgeLengthInSlice);
                size_t domainEndJ = domainStartJ + static_cast<size_t>(domainEdgeLengthInSlice);

                // Iterate over domain corresponding to pos (i,j,k) in downsampled image and sum over the domain
                size_t avgBase = 0;
                float voxelSum = 0.0f;
                for(size_t I=domainStartI; I<domainEndI; I++)
                {
                    if(I >= static_cast<size_t>(numColumns)) continue;
                    for(size_t J=domainStartJ; J<domainEndJ; J++)
                    {
                        if(J >= static_cast<size_t>(numRows)) continue;
                        for(size_t K=domainStartK; K<domainEndK; K++)
                        {
                            if(K >= static_cast<size_t>(numSlices)) continue;
                            voxelSum+= inputVolume[K][J][I];

                            avgBase++;
                        }
                    }
                }

                // Calculate average and store in downsampled image
                if(avgBase > 0) sliceDownsampled[j][i] = voxelSum / static_cast<float>(avgBase);
            }
        }
    }

    return downsampledVolume;
}

///
/// \brief Copies the DICOM headers from each slice from the source to the target volume.
/// \param from [in] Source volume.
/// \param to [out] Target volume.
///
void VolOperations::copyDicomHeaders(const Volume& from, Volume& to)
{
    for(size_t i=0; i<from.getSliceCount(); i++)
    {
        if(i >= to.getSliceCount()) break;

        to[i].setSliceHeader(from[i].getSliceHeader());
    }
}
