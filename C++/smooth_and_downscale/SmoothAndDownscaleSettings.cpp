#include <Utils/NNLDialogDescriptor/Group.h>
#include <Utils/NNLDialogDescriptor/EditBoxInteger.h>
#include <Utils/NNLDialogDescriptor/DropList.h>
#include <Utils/NNLDialogDescriptor/DropListItem.h>
#include <Utils/NNLDialogDescriptor/CheckBox.h>
#include <PluginLibMisc/JSONResultBuilder.h>
#include <PluginLibMisc/Plot2D.h>
#include <PluginLibMisc/PlainText.h>
#include "SmoothAndDownscaleSettings.h"
#include "AlgSmoothing/GaussianConvolution2D.h"
#include "AlgSmoothing/GaussianConvolution3D.h"
#include "AlgDownsample/DownsampleCuboid.h"
#include "AlgDownsample/DownsampleSquare.h"

///
/// \brief Constructor.
/// \param reinitDialogDescription [in] If true, the dialog descriptor is reinitialized. This should be false if constructor is used to generate default settings within buildDialogDescriptor().
///
SmoothAndDownscaleSettings::SmoothAndDownscaleSettings(bool reinitDialogDescription)
{
    resetToDefaults();
    registerModules();
    if(reinitDialogDescription) buildDialogDescriptor();
}

///
/// \brief Reset to default values.
///
void SmoothAndDownscaleSettings::resetToDefaults()
{
    performSmoothing_ = true;
    performDownscale_ = false;
    rangeToSmoothAndDownscale_ = std::pair<int,int>(0, -1);
    seriesToSmoothAndDownscale_ = -1;

    smoothingAlg_ = "GaussianConvolution3D";
    downsampleAlg_ = "DownsampleSquare";
}

///
/// \brief Generates list of modules that are to be available within the plugin.
///
/// Each partial calculation, which can be done in several different ways, that is to be performed within a plugin module can be made with a standard interface.
/// The base class for such algorithms are named AlgBase... and their derivatives are registered within this method. Registration is done by simply making one list
/// for each partial calculation, where each list contains instances of classes that handle the calculation in a different manner.
///
void SmoothAndDownscaleSettings::registerModules()
{
    // Registered smoothing algorithms
    registeredSmoothingAlgorithms_.push_back(std::make_shared<GaussianConvolution2D>());
    registeredSmoothingAlgorithms_.push_back(std::make_shared<GaussianConvolution3D>());

    // Registered downsample algorithms
    registeredDownsampleAlgorithms_.push_back(std::make_shared<DownsampleCuboid>());
    registeredDownsampleAlgorithms_.push_back(std::make_shared<DownsampleSquare>());
}

///
/// \brief This class bulids a NNLDialogDescriptor instance that is later used for generating settings description JSON strings, as well as for parsing and validating JSON settings strings.
///
void SmoothAndDownscaleSettings::buildDialogDescriptor()
{
    SmoothAndDownscaleSettings defaults(false);

    NNLDialogDescriptor dialog("SmoothingAndDownscale", "Smoothing and downscale","Performs smoothing and / or downscaling of input data.");

    std::shared_ptr<Descriptor::Group> groupGeneral = dialog.addGroup("General", "General", "General settings.");
    std::shared_ptr<Descriptor::Group> groupOutput = dialog.addGroup("GroupOutput", "Output", "Choice of parameters to output from the motion correction procedure.");

    // Set up general group
    {
        groupGeneral->addEditBoxInteger(Descriptor::EditBoxInteger(&seriesToSmoothAndDownscale_, "SeriesToSmoothAndScale", "Series index", "Select a series index to smooth / downscale. If set to -1, all input series are smoothed / downsampled.", defaults.seriesToSmoothAndDownscale_, "", { "0" }));
        groupGeneral->addEditBoxInteger(Descriptor::EditBoxInteger(&rangeToSmoothAndDownscale_.first, "FirstVolumeIndex", "First volume index", "First volume index to smooth / downscale.", defaults.rangeToSmoothAndDownscale_.first, "", { "0" }));
        groupGeneral->addEditBoxInteger(Descriptor::EditBoxInteger(&rangeToSmoothAndDownscale_.second, "LastVolumeIndex", "Last volume index", "Last volume index to smooth / downscale. If -1, the last index will be the last of the volume", defaults.rangeToSmoothAndDownscale_.second, "", { "-1" }));
    }

    // Set up output group
    {
        // Set up checkbox, including options for the check box
        {
            NNLDialogDescriptor options("PerformSmoothingOpt", "Options for smoothing", "Options for smoothing.");
            std::shared_ptr<Descriptor::Group> group = options.addGroup("SelectAlgorithms", "Algorithms", "Select algorithms.");
            std::shared_ptr<Descriptor::DropList> dropList = group->addDropList(Descriptor::DropList(&smoothingAlg_, false, "SmoothingAlgorithm", "Smoothing algorithm", "Smoothing algorithm.", defaults.smoothingAlg_));
            dropList->addItems(getDropListItemsFromRegAlgorithms(registeredSmoothingAlgorithms_));
            groupOutput->addCheckBox(Descriptor::CheckBox(&performSmoothing_, "PerformSmoothing", "Perform smoothing", "If set, smoothing is performed", defaults.performSmoothing_, &options, nullptr));
        }

        // Set up checkbox, including options for the check box
        {
            NNLDialogDescriptor options("PerformDownscalingOpt", "Options for downscaling", "Options for downscaling.");
            std::shared_ptr<Descriptor::Group> group = options.addGroup("SelectAlgorithms", "Algorithms", "Select algorithms.");
            std::shared_ptr<Descriptor::DropList> dropList = group->addDropList(Descriptor::DropList(&downsampleAlg_, false, "DownsamplingAlgorithm", "Downsampling algorithm", "Downsampling algorithm.", defaults.downsampleAlg_));
            dropList->addItems(getDropListItemsFromRegAlgorithms(registeredDownsampleAlgorithms_));
            groupOutput->addCheckBox(Descriptor::CheckBox(&performDownscale_, "PerformDownscaling", "Perform downscaling", "If set, downscaling is performed", defaults.performDownscale_, &options, nullptr));
        }
    }

    dialogDescriptor_ = dialog;
}

///
/// \brief Iterate through the registered algorithms to find the algorithm with name 'name'.
/// \param name [in] Name of algoritm to search for.
/// \param regAlgorithms [in] Registered algorithms of type T.
/// \return Pointer to algorithm instance, or nullptr if not found.
///
template<class T>
T* SmoothAndDownscaleSettings::findAlg(QString name, std::vector<std::shared_ptr<T>>& regAlgorithms)
{
    for(auto& item : regAlgorithms)
    {
        auto* p = item.get();
        if(p)
        {
            if(p->getID() == name) return p;
        }
    }

    return nullptr;
}

///
/// \brief Retrieve list of drop list items from the list of algorithms of type T.
/// \param regAlgorithms [in] list of pointers to registered algorithms of type T.
/// \return List of droplist items, with options (when available).
///
template<class T>
std::vector<Descriptor::DropListItem> SmoothAndDownscaleSettings::getDropListItemsFromRegAlgorithms(const std::vector<std::shared_ptr<T>>& regAlgorithms)
{
    std::vector<Descriptor::DropListItem> dropListItems;

    for(size_t i=0; i<regAlgorithms.size(); i++)
    {
        auto ptrToAlg = regAlgorithms[i];
        if(ptrToAlg.get())
        {
            dropListItems.emplace_back((*ptrToAlg).getDropListItem());
        }
    }

    return dropListItems;
}

///
/// \brief Generates a JSON settings description string.
/// \return JSON string.
///
std::string SmoothAndDownscaleSettings::getDescription()
{
    return dialogDescriptor_.getSettingsDescription();
}

///
/// \brief Validates a JSON settings string.
/// \param settings [in] Settings string to validate.
/// \return Returns true if OK, else returns false.
///
bool SmoothAndDownscaleSettings::validate(const std::string& settings)
{
    return dialogDescriptor_.validate(settings).first;
}

///
/// \brief Parses a JSON settings string and updates the related settings variables that was linked to during the buildDialogDescriptor() call.
/// \param settings [in] Settings string to parse.
/// \return Returns true if OK, else returns false.
///
bool SmoothAndDownscaleSettings::parse(const std::string& settings)
{
    if(!validate(settings)) return false;
    if(!dialogDescriptor_.parse(settings)) return false;

    lastSuccessfullyParsedSettings_ = settings.c_str();

    return true;
}

///
/// \brief Generates the JSON string to return from the plugin module (returned through the result of the process call).
/// \param timeCurvesBefore [in] A time curve to include in the output (should be the one taken before processing).
/// \param timeCurvesAfter [in] A time curve to include in the output (should be the one taken after processing).
/// \return Returns the JSON results string to return. Contains both applied settings JSON and results JSON embedded within the same JSON string.
///
std::string SmoothAndDownscaleSettings::genJSONResult(const std::vector<std::pair<QString, std::vector<std::pair<float,float>>>>& timeCurvesBefore,
                                                      const std::vector<std::pair<QString, std::vector<std::pair<float,float>>>>& timeCurvesAfter)
{
    JSONResultBuilder jsonResultBuilder(lastSuccessfullyParsedSettings_);

    for(auto& tc : timeCurvesBefore)
    {
        GfxDataContainers::Plot2D plot2D(tc.second);
        jsonResultBuilder.addToResult(tc.first, plot2D);
    }

    for(auto& tc : timeCurvesAfter)
    {
        GfxDataContainers::Plot2D plot2D(tc.second);
        jsonResultBuilder.addToResult(tc.first, plot2D);
    }

    GfxDataContainers::PlainText plainText1("Number of input time curves", QString("%1").arg(timeCurvesBefore.size()));
    GfxDataContainers::PlainText plainText2("Number of output time curves", QString("%1").arg(timeCurvesAfter.size()));
    jsonResultBuilder.addToResult("NumInputTC", plainText1);
    jsonResultBuilder.addToResult("NumOutputTC", plainText2);

    return jsonResultBuilder.getResultJSONString().toStdString();
}

///
/// \brief Return the selected (downsampleAlg_) downsampling algorithm.
/// \return Pointer to algorithm, or nullptr if it does not exist.
///
AlgBaseDownsample* SmoothAndDownscaleSettings::getDownsampleAlg()
{
    return findAlg<AlgBaseDownsample>(downsampleAlg_, registeredDownsampleAlgorithms_);
}

///
/// \brief Return the selected (smoothingAlg_) smoothing algorithm.
/// \return Pointer to algorithm, or nullptr if it does not exist.
///
AlgBaseSmoothing* SmoothAndDownscaleSettings::getSmoothingAlg()
{
    return findAlg<AlgBaseSmoothing>(smoothingAlg_, registeredSmoothingAlgorithms_);
}

///
/// \brief Returns true if smoothing is to be performed.
/// \return Returns true if smoothing is to be performed, else it returns false.
///
bool SmoothAndDownscaleSettings::performSmoothing()
{
    return performSmoothing_;
}

///
/// \brief Returns true if downscaling is to be performed.
/// \return Returns true if downscaling is to be performed, else it returns false.
///
bool SmoothAndDownscaleSettings::performDownscale()
{
    return performDownscale_;
}

///
/// \brief Get range of volumes to downscale and / or smooth.
/// \return Range of volumes (first volume index, last volume index). Indices are zero based.
///
std::pair<int,int> SmoothAndDownscaleSettings::getRangeToSmoothAndDownscale()
{
    return rangeToSmoothAndDownscale_;
}

///
/// \brief Get the series index to downscale.
/// \return Zero based index of series (if -1, all series are to be processed).
///
int SmoothAndDownscaleSettings::getSeriesToSmoothAndDownscale()
{
    return seriesToSmoothAndDownscale_;
}

