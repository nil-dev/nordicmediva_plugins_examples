#pragma once
#include <PluginInterface/PluginInterface.h>
#include <vector>
#include <memory>
#include <string>
#include "SmoothAndDownscaleSettings.h"

/// \defgroup SmoothAndDownscale Smoothing and downscale plugin.
/// \ingroup SmoothAndDownscale
/// @{
///     \defgroup SmoothAndDownscale_AlgDownsample Algorithms for downsampling
///     \defgroup SmoothAndDownscale_AlgSmoothing Algorithms for smoothing
///     \defgroup SmoothAndDownscale_UtilityClasses Utility classes
/// @}

///
/// \ingroup SmoothAndDownscale
/// \brief This class inherits from and implements PluginInterface.
///
/// This smooth and downscale plugin performs smoothing, downscaling or both, in that order.
///
class SmoothAndDownscale : public PluginInterface
{
public:
    SmoothAndDownscale();
    virtual ~SmoothAndDownscale() override;

public:
    virtual SInterfaceID getInterfaceID() override;
    virtual SSettingsDescription getSettingsDescription() override;
    virtual std::vector<std::string> getProgressIndicators() override;
    virtual bool validateSettings(const std::string& settings) override;
    void abort() override;
    virtual bool willInputDataObjChange() override;

    virtual bool processData(PluginDataObj& inputDataObj, const std::map<std::string, PluginDataObj>& mapOfProcessedDataObjects,
                             const std::string& currentRouteSettings, const std::string& currentQueueSettings, PluginDataObj& result,
                             std::function<void(std::string,int)>& addLogEntry,
                             std::function<void(std::vector<SProgress>&)>& reportProgress,
                             std::function<void(std::string,Series&)>& reportPartialResult) override;

private:
    SmoothAndDownscaleSettings settings_;
    bool abort_;
};


extern "C" BOOST_SYMBOL_EXPORT PluginFactory<SmoothAndDownscale> matt_plugin;
PluginFactory<SmoothAndDownscale> matt_plugin;
