#include <Utils/NNLDialogDescriptor.h>
#include <Utils/NNLDialogDescriptor/Group.h>
#include <Utils/NNLDialogDescriptor/EditBoxInteger.h>
#include "UtilityClasses/Smoothing.h"
#include "UtilityClasses/VolOperations.h"
#include "DownsampleSquare.h"

///
/// \brief Constructor..
///
DownsampleSquare::DownsampleSquare() : AlgBaseDownsample()
{
    domainEdgeLength_ = 2;
    reduceToSingleSlice_ = true;
    selectedSlice_ = -1;
}

///
/// \brief Invoke downsample algorithm.
/// \param inputVolume [in] Volume to apply the algorithm to.
/// \param interpolator [in] Interpolator to assign to output. Mey be nullptr.
/// \return Pointer to downsampled volume.
///
std::shared_ptr<Volume> DownsampleSquare::invoke(const Volume& inputVolume)
{
    return VolOperations::downsample(inputVolume, domainEdgeLength_, 1);
}

///
/// \brief Returns name that uniquely identifies the module.
/// \return String containing name.
///
QString DownsampleSquare::getID()
{
    return "DownsampleSquare";
}

///
/// \brief Returns description of possible settings, their types, as well as default values.
/// \return Drop list item. Returned object also contains pointers to settings variables within the current instance of the object.
///
Descriptor::DropListItem DownsampleSquare::getDropListItem()
{
    DownsampleSquare defaults;

    Descriptor::DropListStringWithOptions dropListStringWithOptions(getID().toStdString(), "Square slice domains", R"(Each set of N by N pixels within each slice, a square slice domain, are combined, through an arithmetic mean, to single pixels in a new set of slices. N is configurable. Note that the slice domains do not overlap.)");
    std::shared_ptr<Descriptor::Group> generalGroup = dropListStringWithOptions.addGroup("GeneralGroup", "General", "Group containing general settings.");
    generalGroup->addEditBoxInteger(Descriptor::EditBoxInteger(&domainEdgeLength_, "DomainEdgeLength", "Length of each domain edge in slice", "Number of voxels to combine into single voxels along each direction of the slice plane.", defaults.domainEdgeLength_, "px", { "1" }));

    return Descriptor::DropListItem(dropListStringWithOptions);
}
