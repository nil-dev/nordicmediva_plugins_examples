#pragma once
#include "AlgBaseDownsample.h"

///
/// \ingroup SmoothAndDownscale_AlgDownsample
/// \brief Downsamples a slices by averaging neighboring 2D squares.
///
/// This algorithm will downsample each slice of the given volume (i.e., it performs two-dimensional downsampling). This
/// is achieved by taking the average of each neighboring domainEdgeLength_*domainEdgeLength_ domain in a slice, starting from the top
/// left corner of the slice, thus yielding one new pixel per domainEdgeLength_*domainEdgeLength_ domain. If the slice resolution is
/// such that either the height or length of the slice is not divisible by domainEdgeLength_, there will be uncomplete domains
/// along the bottom and / or right of the slices. One new average pixel will be generated from each such incomplete domain
/// by averaging over the available pixels within such domains.
///
class DownsampleSquare : public AlgBaseDownsample
{
public:
    DownsampleSquare();

public:
    std::shared_ptr<Volume> invoke(const Volume& inputVolume) override;
    QString getID() override;
    Descriptor::DropListItem getDropListItem() override;

private:
    int domainEdgeLength_;
    bool reduceToSingleSlice_;
    int selectedSlice_;
};
