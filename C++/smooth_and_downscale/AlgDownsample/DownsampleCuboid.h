#pragma once
#include "AlgBaseDownsample.h"

///
/// \ingroup SmoothAndDownscale_AlgDownsample
/// \brief Downsamples a volume by averaging neighboring 3D cuboids.
///
/// This algorithm will downsample the entire given volume (i.e., it performs three-dimensional downsampling). This
/// is achieved by taking the average of each neighboring domainEdgeLengthInSlice_*domainEdgeLengthInSlice_*domainHeight_
/// domain in the volume, starting from the top-left-back corner of the volume, thus yielding one new voxel per
/// domainEdgeLengthInSlice_*domainEdgeLengthInSlice_*domainHeight_ domain. If the slice resolution is
/// such that either the height or length of the slice is not divisible by domainEdgeLengthInSlice_ or domainHeight_, there will
/// be uncomplete domains along some or all of the borders of the volume. One new average voxel will be generated from each such
/// incomplete domain by averaging over the available pixels within such domains.
///
class DownsampleCuboid : public AlgBaseDownsample
{
public:
    DownsampleCuboid();

public:
    std::shared_ptr<Volume> invoke(const Volume& inputVolume) override;
    QString getID() override;
    Descriptor::DropListItem getDropListItem() override;

private:
    int domainEdgeLengthInSlice_;
    int domainHeight_;
};
