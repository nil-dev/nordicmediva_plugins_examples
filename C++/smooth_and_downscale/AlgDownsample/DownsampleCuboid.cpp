#include <Utils/NNLDialogDescriptor.h>
#include <Utils/NNLDialogDescriptor/Group.h>
#include <Utils/NNLDialogDescriptor/EditBoxInteger.h>
#include <Utils/NNLDialogDescriptor/EditBoxString.h>
#include "DownsampleCuboid.h"
#include "UtilityClasses/VolOperations.h"

///
/// \brief Constructor.
///
DownsampleCuboid::DownsampleCuboid() : AlgBaseDownsample()
{
    domainEdgeLengthInSlice_ = 2;
    domainHeight_ = 2;
}

///
/// \brief Invoke downsample algorithm.
/// \param inputVolume [in] Volume to apply the algorithm to.
/// \param interpolator [in] Interpolator to assign to output. Mey be nullptr.
/// \return Pointer to downsampled volume.
///
std::shared_ptr<Volume> DownsampleCuboid::invoke(const Volume& inputVolume)
{
    return VolOperations::downsample(inputVolume, domainEdgeLengthInSlice_, domainHeight_);
}

///
/// \brief Returns name that uniquely identifies the module.
/// \return String containing name.
///
QString DownsampleCuboid::getID()
{
    return "DownsampleCuboid";
}

///
/// \brief Returns description of possible settings, their types, as well as default values.
/// \return Drop list item. Returned object also contains pointers to settings variables within the current instance of the object.
///
Descriptor::DropListItem DownsampleCuboid::getDropListItem()
{
    DownsampleCuboid defaults;

    Descriptor::DropListStringWithOptions dropListStringWithOptions(getID().toStdString(), "Cuboid volume domains", R"(Each set of N by N by M voxels, a cuboid domain, are combined, through an arithmetic mean, to single voxels in a new volume. N and M are configurable. Note that the cuboids do not overlap.)");
    std::shared_ptr<Descriptor::Group> generalGroup = dropListStringWithOptions.addGroup("GeneralGroup", "General", "Group containing general settings.");
    generalGroup->addEditBoxInteger(Descriptor::EditBoxInteger(&domainEdgeLengthInSlice_, "DomainEdgeLengthInSlice", "Length of each domain edge in slice", "Number of voxels to combine into single voxels along each direction of the slice plane.", defaults.domainEdgeLengthInSlice_, "px", { "1" }));
    generalGroup->addEditBoxInteger(Descriptor::EditBoxInteger(&domainHeight_, "DomainHeight", "Height of each domain", "Number of voxels to combine into single voxels along the hieght of the volume.", defaults.domainHeight_, "px", { "1" }));

    return Descriptor::DropListItem(dropListStringWithOptions);
}
