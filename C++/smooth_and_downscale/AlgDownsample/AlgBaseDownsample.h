#pragma once
#include <QString>
#include <memory>
#include <Series/Series.h>
#include <Utils/NNLDialogDescriptor/DropListItem.h>

///
/// \ingroup SmoothAndDownscale_AlgDownsample
/// \brief Base class for all downsampling algorithms.
///
class AlgBaseDownsample
{
public:
    AlgBaseDownsample();
    virtual ~AlgBaseDownsample();

public:
    virtual std::shared_ptr<Volume> invoke(const Volume& inputVolume) = 0;
    virtual QString getID() = 0;
    virtual Descriptor::DropListItem getDropListItem() = 0;
};
