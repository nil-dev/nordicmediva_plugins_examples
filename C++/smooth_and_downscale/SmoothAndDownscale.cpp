#include "SmoothAndDownscale.h"
#include "Version.h"
#include "UtilityClasses/VolOperations.h"
#include "UtilityClasses/Plots.h"
#include <QString>
#include <SeriesCreator/CreateSeries.h>
#include <PluginInterface/LogLevelDef.h>

///
/// \brief Constructor.
///
SmoothAndDownscale::SmoothAndDownscale()
{
    abort_ = false;
}

///
/// \brief Destructor.
///
SmoothAndDownscale::~SmoothAndDownscale()
{

}

///
/// \brief Returns interface details.
/// \return Name, description, UUID and version information.
///
PluginInterface::SInterfaceID SmoothAndDownscale::getInterfaceID()
{
    PluginInterface::SInterfaceID ret;

    ret.name_ = "SmoothingAndDownscale";
    ret.description_ = "This plugin smooths and / or downscales the input data.";
    ret.UUID_ = "31d2d015-0677-457c-bc98-4840e23ff772";
    ret.versionMajor_ = TEMPLPLUG_VER_MAJOR;
    ret.versionMinor_ = TEMPLPLUG_VER_MINOR;
    ret.versionRevision_ = TEMPLPLUG_VER_REVISION;

    return ret;
}

///
/// \brief Returns information about how the plugin can be configured.
/// \return JSON formated string and the number of Series required as input to the plugin (-1, since it can accept
/// one or more Series into the PluginDataObj handed to processData()).
///
PluginInterface::SSettingsDescription SmoothAndDownscale::getSettingsDescription()
{
    PluginInterface::SSettingsDescription ret;

    ret.description_ = settings_.getDescription();
    ret.requiredNumSeries_ = -1;

    return ret;
}

///
/// \brief Returns descriptions for all progress indicators.
/// \return List of progress indicators.
///
/// Each progess indicator will ideally result in one progress bar per progress indicator in a visual environment running the plugin module.
///
std::vector<std::string> SmoothAndDownscale::getProgressIndicators()
{
    std::vector<std::string> progressIndicators;

    progressIndicators.push_back("Series");
    progressIndicators.push_back("Smoothing");
    progressIndicators.push_back("Downscaling");

    return progressIndicators;
}

///
/// \brief Validates a JSON configuration string.
/// \param settings [in] JSON configuration string.
/// \return true if validation is OK, else returns false.
///
bool SmoothAndDownscale::validateSettings(const std::string& settings)
{
    return settings_.validate(settings);
}

///
/// \brief Aborts the image export.
///
/// This method can be called from a separate thread than the one executing processData(). It
/// will set abort flags to true, while the loops within processData() method will check these flags
/// at differnt points during execution (and exit loops if true).
///
void SmoothAndDownscale::abort()
{
    abort_ = true;
}

///
/// \brief States wether inputDataObj of processData() will be modified.
/// \return false (i.e., inputDataObj will not be modified).
///
bool SmoothAndDownscale::willInputDataObjChange()
{
    return false;
}

///
/// \brief Performs smoothing and / or downscaling on input dataset and returns a processed dataset as result.
/// \param inputDataObj [in] Instance to contain one or more series to be processed.
/// \param mapOfProcessedDataObjects [in] Not in use.
/// \param currentRoute [in] Not in use.
/// \param result [out] Contains single Series, with processed data.
/// \param addLogEntry [out] Callback function used to report log entries (in the form of strings and severity).
/// \param reportProgress [out] Callback function used to report progress (see getProgressIndicators() for more info.).
/// \param reportPartialResult [out] Not in use.
/// \return true if OK, else returns false.
///
bool SmoothAndDownscale::processData(PluginDataObj& inputDataObj, const std::map<std::string, PluginDataObj>&,
                                 const std::string&, const std::string&, PluginDataObj& result, std::function<void(std::string,int)>& addLogEntry,
                                 std::function<void(std::vector<SProgress>&)>& reportProgress, std::function<void(std::string,Series&)>&)
{
    abort_ = false;

    // Add to log that we started
    addLogEntry("Begining data processing...", LogLevels::logStatus);

    // Parse input settings
    if(!settings_.parse(inputDataObj.getSettings()))
    {
        addLogEntry("Could not parse settings!", LogLevels::logCriticalError);
        return false;
    }

    // Retrieve selected algorithms
    AlgBaseSmoothing* smootingAlg = settings_.getSmoothingAlg();
    AlgBaseDownsample* downsampleAlg = settings_.getDownsampleAlg();

    // Retrieve indices of series to process, based on settings
    std::vector<size_t> listOfSeries;
    if(settings_.getSeriesToSmoothAndDownscale() == -1)
    {
        for(size_t i=0; i<inputDataObj.getNumSeries(); i++)
        {
            listOfSeries.emplace_back(i);
        }
    }
    else
    {
        if(settings_.getSeriesToSmoothAndDownscale() < static_cast<int>(inputDataObj.getNumSeries()))
        {
            listOfSeries.emplace_back(settings_.getSeriesToSmoothAndDownscale());
        }
    }

    if(listOfSeries.size() <= 0)
    {
        addLogEntry("None of the selected series were found!", LogLevels::logCriticalError);
        return false;
    }

    // Set up vector used to report progress (we set up three progress indicators in the getProgressIndicators() call)
    std::vector<SProgress> progressVec;
    progressVec.emplace_back(SProgress(0, static_cast<int>(listOfSeries.size())));
    progressVec.emplace_back(SProgress(0, 100));
    progressVec.emplace_back(SProgress(0, 100));
    reportProgress(progressVec);

    // Perform smoothing and downscaling, if selected and available
    std::pair<int,int> range = settings_.getRangeToSmoothAndDownscale();
    for(size_t i=0; i<listOfSeries.size(); i++)
    {
        std::shared_ptr<Series> inputSeries = inputDataObj.getSeries(listOfSeries[i]).first;

        // Create a series object with the same dimensions as the input to use as output
        std::shared_ptr<Series> outputSeries = SeriesCreator::createEmptySeries(inputSeries);

        // Retreive start and end volume indices and make sure they are within the range of the input data
        size_t startVol = static_cast<size_t>(range.first);
        size_t endVol = (range.second <= -1) ? inputSeries->getVolumeCount() - 1 : static_cast<size_t>(range.second);

        if(startVol >= inputSeries->getVolumeCount())
        {
            startVol = inputSeries->getVolumeCount() - 1;
            addLogEntry("Start volume was beyond last volume of series and was set to the last volume of the series!", LogLevels::logWarning);
        }

        if(endVol >= inputSeries->getVolumeCount())
        {
            endVol = inputSeries->getVolumeCount() - 1;
            addLogEntry("End volume was beyond last volume of series and was set to the last volume of the series!", LogLevels::logWarning);
        }

        // Go through the selected volumes and perform smoothing
        int progressIndex = 0;
        int progressCount = static_cast<int>(endVol) - static_cast<int>(startVol) + 1;
        for(size_t v=startVol; v<=endVol; v++)
        {
            std::shared_ptr<Volume> outputVol = inputSeries->getVolumes()[v];
            progressIndex++;

            // Perform smoothing if selected and available
            if(abort_) break;
            if(settings_.performSmoothing() && smootingAlg)
            {
                 outputVol = smootingAlg->invoke(*outputVol);
            }

            // Update progress for smoothing
            progressVec[1] = SProgress(progressIndex, progressCount);
            reportProgress(progressVec);

            // Perform downsampling if selected and available
            if(abort_) break;
            if(settings_.performDownscale() && downsampleAlg)
            {
                 outputVol = downsampleAlg->invoke(*outputVol);
            }

            // Update progress for downscaling
            progressVec[2] = SProgress(progressIndex, progressCount);
            reportProgress(progressVec);

            // Store results
            outputSeries->getVolumes()[v] = outputVol;
        }

        // Add output series (only show series that have been altered)
        result.addSeries(QString("OutputSeries%1").arg(listOfSeries[i]).toStdString(), outputSeries);

        // Update main progress
        progressVec[0] = SProgress(static_cast<int>(i+1), static_cast<int>(listOfSeries.size()));
        reportProgress(progressVec);
    }

    // Copy DICOM headers
    for(size_t i=0; i<result.getNumSeries(); i++)
    {
        for(size_t j=0; j<result[i]->getVolumeCount(); j++)
        {
            VolOperations::copyDicomHeaders((*inputDataObj[i])[j], (*result[i])[j]);
        }
    }

    // Create JSON output string
    std::vector<std::pair<QString, std::vector<std::pair<float,float>>>> timeCurvesBefore(inputDataObj.getNumSeries());
    std::vector<std::pair<QString, std::vector<std::pair<float,float>>>> timeCurvesAfter(result.getNumSeries());

    for(size_t i=0; i<inputDataObj.getNumSeries(); i++)
    {
        std::pair<std::shared_ptr<Series>, std::string> series = inputDataObj.getSeries(i);
        timeCurvesBefore[i].second = Plots::genTimeCurve(*series.first);
        timeCurvesBefore[i].first = QString("Time curve %1 before").arg(series.second.c_str());
    }

    for(size_t i=0; i<result.getNumSeries(); i++)
    {
        std::pair<std::shared_ptr<Series>, std::string> series = result.getSeries(i);
        timeCurvesAfter[i].second = Plots::genTimeCurve(*series.first);
        timeCurvesAfter[i].first = QString("Time curve %1 after").arg(series.second.c_str());
    }

    result.setSettings(settings_.genJSONResult(timeCurvesBefore, timeCurvesAfter));

    // Report differently if process was aborted
    if(abort_)
    {
        addLogEntry("Processing aborted!", LogLevels::logStatus);
    }
    else
    {
        addLogEntry("Processing complete!", LogLevels::logStatus);
    }

    // Provide a final progress update
    progressVec[0] = SProgress(100, 100);
    progressVec[1] = SProgress(100, 100);
    progressVec[2] = SProgress(100, 100);
    reportProgress(progressVec);

    return true;
}
