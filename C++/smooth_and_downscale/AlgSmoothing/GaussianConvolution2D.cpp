#include <Utils/NNLDialogDescriptor.h>
#include <Utils/NNLDialogDescriptor/Group.h>
#include <Utils/NNLDialogDescriptor/EditBoxFloat.h>
#include <Utils/NNLDialogDescriptor/DropListString.h>
#include "UtilityClasses/Smoothing.h"
#include "UtilityClasses/VolOperations.h"
#include "GaussianConvolution2D.h"

///
/// \brief Constructor.
///
GaussianConvolution2D::GaussianConvolution2D()
{
    stdDeviation_ = 1.0;
}

///
/// \brief Invoke smoothing algorithm.
/// \param inputVolume [in] Volume to apply the algorithm to.
/// \return Pointer to smoothed volume.
///
/// The smoothing is performed by convoluting the images of a slice by a Gaussian kernel. Let the kernel be
/// \f[G(x,y) \equiv \left[ \frac{1}{\sqrt{2\pi\sigma^2}} \right]^2 e^{-\frac{x^2 + y^2}{2\sigma^2}} = g(x)g(y),\f]
/// where
/// \f[g(x) \equiv \frac{1}{\sqrt{2\pi\sigma^2}} e^{-\frac{x^2}{2\sigma^2}}, \f]
/// with \f$\sigma\f$ being the standard deviation.
///
/// Now, let \f$f(x,y)\f$ represent a slice image in continous space. For each discrete pixel in the image, we wish
/// to 'mix' in the surrounding neighbooring pixels by linearly combining them with Gaussian weights. In continous
/// space, this yields the smoothed image,
/// \f[F(x,y) = \int_0^w d\tilde{x} \int_0^h d\tilde{y} G(\tilde{x} - x, \tilde{y} - y) f(\tilde{x}, \tilde{y}) \equiv f * G, \f]
/// which is a convolution, where \f$w\f$ and \f$h\f$ are the height and width of the image, respectively. We now define
/// \f$f(x,y)\f$ to vanish outside the domain \f$[0,w]\times[0,h]\f$. Moreover, the Gaussian becomes very small in a radius
/// of \f$\pm 3\sigma\f$ away from the central pixel of a convolution step. Thus, we have that
/// \f[F(x,y) \approx \int_{x-3\sigma}^{x+3\sigma} d\tilde{x} \int_{y-3\sigma}^{y+3\sigma} d\tilde{y} f(\tilde{x}, \tilde{y}) G(\tilde{x} - x, \tilde{y} - y). \f]
/// In the discrete case, where the integrals become sums, this yields a time complexity of \f$\mathcal{O}(N_wN_h\lceil 6\sigma \rceil^2)\f$.
/// This time complexity, with respect to the standard deviation, can be reduced by instead writing
/// \f[F(x,y) \approx \int_{x-3\sigma}^{x+3\sigma} d\tilde{x} \left[ \int_{y-3\sigma}^{y+3\sigma} d\tilde{y} f(\tilde{x}, \tilde{y}) g(\tilde{y} - y) \right] g(\tilde{x} - x). \f]
/// Thus, we see that the factor
/// \f[F_1(y,\tilde{x}) \equiv \int_{y-3\sigma}^{y+3\sigma} d\tilde{y} f(\tilde{x}, \tilde{y}) g(\tilde{y} - y)\f]
/// can be computed beforehand, with a time complexity of \f$\mathcal{O}(N_hN_w\lceil 6\sigma \rceil),\f$
/// hence giving a total time complexity of \f$\mathcal{O}(N_hN_w\lceil 6\sigma \rceil + N_hN_w\lceil 6\sigma \rceil) \approx \mathcal{O}(N_hN_w\lceil 6\sigma \rceil) \f$
/// Finally, the expressions to be evaluated are (in the discretized domain)
/// \f[F^1_{n,\tilde{m}} = \sum_{\tilde{n}=n-\lceil 3\sigma \rceil}^{\tilde{n}=n+\lceil 3\sigma \rceil} f_{\tilde{m},\tilde{n}} \cdot g_{\tilde{n}-n}\f]
/// and then
/// \f[F_{m,n} = \sum_{\tilde{m}=m-\lceil 3\sigma \rceil}^{\tilde{m}=m+\lceil 3\sigma \rceil} F^1_{n,\tilde{m}} \cdot g_{\tilde{m}-m}.\f]
/// It should be noted that, in the discrete case, the units of \f$\sigma, m, n\f$ are all in pixels. Moreover, the kernel \f$g_{n}\f$ is valid for \f$n\f$ between
/// \f$n=-\lceil 3\sigma \rceil\f$ and \f$n=\lceil 3\sigma \rceil\f$, which requires negative indices. We may define a more convenient
/// kernel, \f$k_m\f$, where \f$m\f$ instead ranges from 0 to \f$n=\lceil 6\sigma \rceil\f$, where the indices are shifted such that \f$m=0\f$ corresponds
/// to \f$n=-\lceil 3\sigma \rceil\f$. Then, we can replace \f$g_n\f$ above, with \f$k_{n+\lceil 3\sigma \rceil}\f$.
///
std::shared_ptr<Volume> GaussianConvolution2D::invoke(const Volume& inputVolume)
{
    // The range is the radius defining the boundary where the Gaussian is calculated and it is +/- 3*sigma.
    int range = static_cast<int>(ceil(3.0 * stdDeviation_));

    // Prepare Gaussian kernel, k
    std::shared_ptr<std::vector<float>> k = Smoothing::calcGaussianKernel(range, stdDeviation_);

    // Create output volume
    int numSlices, numColumns, numRows;
    if(!VolOperations::getVolumeSizes(inputVolume, numSlices, numColumns, numRows)) return nullptr;
    std::shared_ptr<Volume> outputVolume = VolOperations::createEmptyVolume(numSlices, numRows, numColumns);

    // Apply Gaussian convolution to all slices in the volume and store to output volume
    for(size_t s=0; s<inputVolume.getSliceCount(); s++)
    {
        // Obtain input and output slices
        const Slice& slice = inputVolume[s];
        Slice& outputSlice = (*outputVolume)[s];

        // Create the matrix to receive the one dimensional convolution result and calculate it
        std::vector<std::vector<float>> F1(static_cast<size_t>(numRows));
        for(auto& array : F1) array.resize(static_cast<size_t>(numColumns), 0.0);
        for(int n=0; n<numRows; n++)
        {
            for(int mt=0; mt<numColumns; mt++)
            {
                for(int nt=(n-range); nt<=(n+range); nt++)
                {
                    int kernIndex = nt - n + range;
                    if((nt < 0) || (nt >= numRows) || (kernIndex < 0) || (kernIndex > (2*range))) continue;

                    F1[static_cast<size_t>(n)][static_cast<size_t>(mt)]+= slice[static_cast<size_t>(nt)][mt] * (*k)[static_cast<size_t>(kernIndex)];
                }
            }
        }

        // Use F1 as 'image' to convolute using g, to create final smoothed image
        for(int m=0; m<numColumns; m++)
        {
            for(int n=0; n<numRows; n++)
            {
                float acc = 0.0f;
                for(int mt=(m-range); mt<=(m+range); mt++)
                {
                    int kernIndex = mt - m + range;
                    if((mt < 0) || (mt >= numColumns) || (kernIndex < 0) || (kernIndex > (2*range))) continue;

                    acc+= F1[static_cast<size_t>(n)][static_cast<size_t>(mt)] * (*k)[static_cast<size_t>(kernIndex)];
                }

                outputSlice[static_cast<size_t>(n)][m] = acc;
            }
        }
    }

    return outputVolume;
}

///
/// \brief Returns name that uniquely identifies the module.
/// \return String containing name.
///
QString GaussianConvolution2D::getID()
{
    return "GaussianConvolution2D";
}

///
/// \brief Returns description of possible settings, their types, as well as default values.
/// \return Drop list item. Returned object also contains pointers to settings variables within the current instance of the object.
///
Descriptor::DropListItem GaussianConvolution2D::getDropListItem()
{
    GaussianConvolution2D defaults;

    Descriptor::DropListStringWithOptions dropListStringWithOptions(getID().toStdString(), "Gaussian convolution - 2D", R"(Perform smoothing of the voxel volumes by smoothing one slice at the time using 2D Gaussian convolution.)");
    std::shared_ptr<Descriptor::Group> generalGroup = dropListStringWithOptions.addGroup("GeneralGroup", "General", "Group containing general settings.");
    generalGroup->addEditBoxFloat(Descriptor::EditBoxFloat(&stdDeviation_, "StdDeviation", "Standard deviation", "Standard deviation for the Gaussian convolution algorithm.", defaults.stdDeviation_, "px", { "0" }));

    return Descriptor::DropListItem(dropListStringWithOptions);
}
