#include <Utils/NNLDialogDescriptor.h>
#include <Utils/NNLDialogDescriptor/Group.h>
#include <Utils/NNLDialogDescriptor/EditBoxFloat.h>
#include <Utils/NNLDialogDescriptor/DropListString.h>
#include "UtilityClasses/Smoothing.h"
#include "UtilityClasses/VolOperations.h"
#include "GaussianConvolution3D.h"

///
/// \brief Constructor.
///
GaussianConvolution3D::GaussianConvolution3D()
{
    stdDeviation_ = 1.0;
}

///
/// \brief Invoke smoothing algorithm.
/// \param inputVolume [in] Volume to apply the algorithm to.
/// \return Pointer to smoothed volume.
///
/// The smoothing is performed by convoluting the images of a slice by a Gaussian kernel. Let the kernel be
/// \f[G(x,y,z) \equiv \left[ \frac{1}{\sqrt{2\pi\sigma^2}} \right]^3 e^{-\frac{x^2 + y^2 + z^2}{2\sigma^2}} = g(x)g(y)g(z),\f]
/// where
/// \f[g(x) \equiv \frac{1}{\sqrt{2\pi\sigma^2}} e^{-\frac{x^2}{2\sigma^2}}, \f]
/// with \f$\sigma\f$ being the standard deviation.
///
/// Now, let \f$f(x,y,z)\f$ represent a volumetric image in continous space. For each discrete pixel in the image, we wish
/// to 'mix' in the surrounding neighbooring pixels by linearly combining them with Gaussian weights. In continous
/// space, this yields the smoothed image,
/// \f[F(x,y,z) = \int_0^w d\tilde{x} \int_0^h d\tilde{y} \int_0^d d\tilde{z} G(\tilde{x} - x, \tilde{y} - y, \tilde{z} - z) f(\tilde{x}, \tilde{y}, \tilde{z}) \equiv f * G, \f]
/// which is a three dimensional convolution, where \f$w\f$, \f$h\f$ and \f$d\f$ are the height, width and depth of the volumetric image, respectively. We now define
/// \f$f(x,y,z)\f$ to vanish outside the domain \f$[0,w]\times[0,h]\times[0,d]\f$. Moreover, the Gaussian becomes very small in a radius
/// of \f$\pm 3\sigma\f$ away from the central pixel of a convolution step. Thus, we have that
/// \f[F(x,y,z) \approx \int_{x-3\sigma}^{x+3\sigma} d\tilde{x} \int_{y-3\sigma}^{y+3\sigma} d\tilde{y} \int_{z-3\sigma}^{z+3\sigma} d\tilde{z} f(\tilde{x}, \tilde{y}, \tilde{z}) G(\tilde{x} - x, \tilde{y} - y, \tilde{z} - z). \f]
/// In the discrete case, where the integrals become sums, this yields a time complexity of \f$\mathcal{O}(N_wN_hN_d\lceil 6\sigma \rceil^3)\f$.
/// This time complexity, with respect to the standard deviation, can be reduced by instead writing
/// \f[F(x,y,z) \approx \int_{x-3\sigma}^{x+3\sigma} d\tilde{x} \left[ \int_{y-3\sigma}^{y+3\sigma} d\tilde{y} \left\{ \int_{z-3\sigma}^{z+3\sigma} d\tilde{z} f(\tilde{x}, \tilde{y}, \tilde{z}) g(\tilde{z} - z) \right\} g(\tilde{y} - y) \right] g(\tilde{x} - x). \f]
/// Thus, we see that the factors
/// \f[F_1(z,\tilde{y},\tilde{x}) \equiv \int_{z-3\sigma}^{z+3\sigma} d\tilde{z} f(\tilde{x}, \tilde{y}, \tilde{z}) g(\tilde{z} - z)\f]
/// and
/// \f[F_2(y,z,\tilde{x}) \equiv  \int_{y-3\sigma}^{y+3\sigma} d\tilde{y} F_1(z,\tilde{y},\tilde{x}) g(\tilde{y} - y) \f]
/// both can be computed beforehand, with a time complexity of \f$\mathcal{O}(N_hN_wN_d\lceil 6\sigma \rceil),\f$
/// hence giving a total time complexity of \f$\mathcal{O}(3N_hN_wN_d\lceil 6\sigma \rceil) \approx \mathcal{O}(N_hN_wN_d\lceil 6\sigma \rceil) \f$
/// Finally, the expressions to be evaluated are (in the discretized domain)
/// \f[F^1_{l,\tilde{n},\tilde{m}} = \sum_{\tilde{l}=l-\lceil 3\sigma \rceil}^{\tilde{l}=l+\lceil 3\sigma \rceil} f_{\tilde{m},\tilde{n},\tilde{l}} \cdot g_{\tilde{l}-l},\f]
/// \f[F^2_{n,l,\tilde{m}} = \sum_{\tilde{n}=n-\lceil 3\sigma \rceil}^{\tilde{n}=n+\lceil 3\sigma \rceil} F^1_{l,\tilde{n},\tilde{m}} \cdot g_{\tilde{n}-n},\f]
/// and then
/// \f[F_{m,n,l} = \sum_{\tilde{m}=m-\lceil 3\sigma \rceil}^{\tilde{m}=m+\lceil 3\sigma \rceil} F^2_{n,l,\tilde{m}} \cdot g_{\tilde{m}-m}.\f]
/// It should be noted that, in the discrete case, the units of \f$\sigma, m, n, l\f$ are all in pixels. Moreover, the kernel \f$g_{n}\f$ is valid for \f$n\f$ between
/// \f$n=-\lceil 3\sigma \rceil\f$ and \f$n=\lceil 3\sigma \rceil\f$, which requires negative indices. We may define a more convenient
/// kernel, \f$k_m\f$, where \f$m\f$ instead ranges from 0 to \f$n=\lceil 6\sigma \rceil\f$, where the indices are shifted such that \f$m=0\f$ corresponds
/// to \f$n=-\lceil 3\sigma \rceil\f$. Then, we can replace \f$g_n\f$ above, with \f$k_{n+\lceil 3\sigma \rceil}\f$.
///
std::shared_ptr<Volume> GaussianConvolution3D::invoke(const Volume& inputVolume)
{
    // The range is the radius defining the boundary where the Gaussian is calculated and it is +/- 3*sigma.
    int range = static_cast<int>(ceil(3.0 * stdDeviation_));

    // Prepare Gaussian kernel, k
    std::shared_ptr<std::vector<float>> k = Smoothing::calcGaussianKernel(range, stdDeviation_);

    // Create output volume
    int numSlices, numColumns, numRows;
    if(!VolOperations::getVolumeSizes(inputVolume, numSlices, numColumns, numRows)) return nullptr;
    std::shared_ptr<Volume> outputVolume = VolOperations::createEmptyVolume(numSlices, numRows, numColumns);

    // Create the first matrix to receive one dimensional convolution results and calculate it
    std::vector<std::vector<std::vector<float>>> F1(static_cast<size_t>(numSlices));
    for(auto& array1 : F1)
    {
        array1.resize(static_cast<size_t>(numRows));
        for(auto& array2 : array1) array2.resize(static_cast<size_t>(numColumns), 0.0f);
    }

    for(int l=0; l<numSlices; l++)
    {
        for(int nt=0; nt<numRows; nt++)
        {
            for(int mt=0; mt<numColumns; mt++)
            {
                for(int lt=(l-range); lt<=(l+range); lt++)
                {
                    int kernIndex = lt - l + range;
                    if((lt < 0) || (lt >= numSlices) || (kernIndex < 0) || (kernIndex > (2*range))) continue;

                    const Slice& slice = inputVolume[static_cast<size_t>(lt)];
                    F1[static_cast<size_t>(l)][static_cast<size_t>(nt)][static_cast<size_t>(mt)]+= slice[static_cast<size_t>(nt)][mt] * (*k)[static_cast<size_t>(kernIndex)];
                }
            }
        }
    }

    // Create the second matrix to receive one dimensional convolution results and calculate it
    std::vector<std::vector<std::vector<float>>> F2(static_cast<size_t>(numRows));
    for(auto& array1 : F2)
    {
        array1.resize(static_cast<size_t>(numSlices));
        for(auto& array2 : array1) array2.resize(static_cast<size_t>(numColumns), 0.0f);
    }

    for(int n=0; n<numRows; n++)
    {
        for(int l=0; l<numSlices; l++)
        {
            for(int mt=0; mt<numColumns; mt++)
            {
                for(int nt=(n-range); nt<=(n+range); nt++)
                {
                    int kernIndex = nt - n + range;
                    if((nt < 0) || (nt >= numRows) || (kernIndex < 0) || (kernIndex > (2*range))) continue;

                    F2[static_cast<size_t>(n)][static_cast<size_t>(l)][static_cast<size_t>(mt)]+= F1[static_cast<size_t>(l)][static_cast<size_t>(nt)][static_cast<size_t>(mt)] * (*k)[static_cast<size_t>(kernIndex)];
                }
            }
        }
    }

    // Use F2 as 'image' to convolute using g, to create final smoothed image
    for(int l=0; l<numSlices; l++)
    {
        Slice& outputSlice = (*outputVolume)[static_cast<size_t>(l)];
        for(int m=0; m<numColumns; m++)
        {
            for(int n=0; n<numRows; n++)
            {
                float acc = 0.0f;
                for(int mt=(m-range); mt<=(m+range); mt++)
                {
                    int kernIndex = mt - m + range;
                    if((mt < 0) || (mt >= numColumns) || (kernIndex < 0) || (kernIndex > (2*range))) continue;

                    acc+= F2[static_cast<size_t>(n)][static_cast<size_t>(l)][static_cast<size_t>(mt)] * (*k)[static_cast<size_t>(kernIndex)];
                }

                outputSlice[static_cast<size_t>(n)][m] = acc;
            }
        }
    }

    return outputVolume;
}

///
/// \brief Returns name that uniquely identifies the module.
/// \return String containing name.
///
QString GaussianConvolution3D::getID()
{
    return "GaussianConvolution3D";
}

///
/// \brief Returns description of possible settings, their types, as well as default values.
/// \return Drop list item. Returned object also contains pointers to settings variables within the current instance of the object.
///
Descriptor::DropListItem GaussianConvolution3D::getDropListItem()
{
    GaussianConvolution3D defaults;

    Descriptor::DropListStringWithOptions dropListStringWithOptions(getID().toStdString(), "Gaussian convolution - 3D", R"(Perform smoothing of the voxel volumes by smoothing in all three directions using 3D Gaussian convolution.)");
    std::shared_ptr<Descriptor::Group> generalGroup = dropListStringWithOptions.addGroup("GeneralGroup", "General", "Group containing general settings.");
    generalGroup->addEditBoxFloat(Descriptor::EditBoxFloat(&stdDeviation_, "StdDeviation", "Standard deviation", "Standard deviation for the Gaussian convolution algorithm.", defaults.stdDeviation_, "px", { "0" }));

    return Descriptor::DropListItem(dropListStringWithOptions);
}
