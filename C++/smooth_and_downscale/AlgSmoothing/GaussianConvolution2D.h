#pragma once
#include <memory>
#include <vector>
#include <QString>
#include <Series/Series.h>
#include "AlgBaseSmoothing.h"

///
/// \ingroup SmoothAndDownscale_AlgSmoothing
/// \brief Performs smoothing of each slice in a volume using 2D Gaussian convolution.
///
class GaussianConvolution2D : public AlgBaseSmoothing
{
public:
    GaussianConvolution2D();

public:
    std::shared_ptr<Volume> invoke(const Volume& inputVolume) override;
    QString getID() override;
    Descriptor::DropListItem getDropListItem() override;

private:
    double stdDeviation_;
};
