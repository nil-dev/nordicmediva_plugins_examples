#pragma once
#include <QString>
#include <memory>
#include <Series/Series.h>
#include <Utils/NNLDialogDescriptor/DropListItem.h>

///
/// \ingroup SmoothAndDownscale_AlgSmoothing
/// \brief Base class for all smoothing algorithms.
///
class AlgBaseSmoothing
{
public:
    AlgBaseSmoothing();
    virtual ~AlgBaseSmoothing();

public:
    virtual std::shared_ptr<Volume> invoke(const Volume& inputVolume) = 0;
    virtual QString getID() = 0;
    virtual Descriptor::DropListItem getDropListItem() = 0;
};
