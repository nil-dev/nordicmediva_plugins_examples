#pragma once

#include <stdio.h>
#include <string>
#include <QString>
#include <Utils/NNLDialogDescriptor.h>
#include <Utils/NNLDialogDescriptor/DropListItem.h>
#include "AlgDownsample/AlgBaseDownsample.h"
#include "AlgSmoothing/AlgBaseSmoothing.h"

///
/// \ingroup SmoothAndDownscale
/// \brief Contains all primary settings for the smooth and downscale plugin, which originates from the provided JSON settings string.
///
/// This class is responsible for both parsing an incoming JSON string, as well as to provide the description (i.e.,
/// required format of the input JSON settings string). Moreover, it contains a method for validating the JSON settings before
/// applying them.
///
/// This class also contains a pointerlist of all registered algorithms, containing one instance per algorithm. These are also registered
/// within this class by pushing instances of the algorithm classes to the vectors of registered algorithms.
///
class SmoothAndDownscaleSettings
{
public:
    SmoothAndDownscaleSettings(bool reinitDialogDescription=true);

public:
    std::string getDescription();
    bool validate(const std::string& settings);
    bool parse(const std::string& settings);
    std::string genJSONResult(const std::vector<std::pair<QString, std::vector<std::pair<float,float>>>>& timeCurvesBefore,
                              const std::vector<std::pair<QString, std::vector<std::pair<float,float>>>>& timeCurvesAfter);

    AlgBaseDownsample* getDownsampleAlg();
    AlgBaseSmoothing* getSmoothingAlg();

    bool performSmoothing();
    bool performDownscale();
    std::pair<int,int> getRangeToSmoothAndDownscale();
    int getSeriesToSmoothAndDownscale();

private:
    void resetToDefaults();
    void registerModules();
    void buildDialogDescriptor();

    template<class T> T* findAlg(QString name, std::vector<std::shared_ptr<T>>& regAlgorithms);
    template<class T> std::vector<Descriptor::DropListItem> getDropListItemsFromRegAlgorithms(const std::vector<std::shared_ptr<T>>& regAlgorithms);

private:
    bool performSmoothing_;
    bool performDownscale_;
    std::pair<int,int> rangeToSmoothAndDownscale_;
    int seriesToSmoothAndDownscale_;

    QString smoothingAlg_;
    QString downsampleAlg_;
    std::vector<std::shared_ptr<AlgBaseDownsample>> registeredDownsampleAlgorithms_;
    std::vector<std::shared_ptr<AlgBaseSmoothing>> registeredSmoothingAlgorithms_;

    NNLDialogDescriptor dialogDescriptor_;
    QString lastSuccessfullyParsedSettings_;
};
