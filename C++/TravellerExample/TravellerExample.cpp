#include "TravellerExample.h"
#include <QString>
#include <SeriesCreator/CreateSeries.h>
#include <PluginInterface/LogLevelDef.h>

///
/// \brief Constructor.
///
TravellerExample::TravellerExample()
{
    abort_ = false;
}

///
/// \brief Destructor.
///
TravellerExample::~TravellerExample()
{

}

///
/// \brief Returns interface details.
/// \return Name, description, UUID and version information.
///
PluginInterface::SInterfaceID TravellerExample::getInterfaceID()
{
    PluginInterface::SInterfaceID ret;

    ret.name_ = settings_.getID();
    ret.description_ = "This plugin does nothing.";
    ret.UUID_ = "deef59bd-659a-4030-981f-d881936aa27f";
    ret.versionMajor_ = 0;
    ret.versionMinor_ = 0;
    ret.versionRevision_ = 1;

    return ret;
}

///
/// \brief Returns information about how the plugin can be configured.
/// \return JSON formated string and the number of Series required as input to the plugin (-1, since it can accept
/// one or more Series into the PluginDataObj handed to processData()).
///
PluginInterface::SSettingsDescription TravellerExample::getSettingsDescription()
{
    
    PluginInterface::SSettingsDescription ret;

    ret.description_ = settings_.getDescription();
        
    ret.requiredNumSeries_ = -1;

    return ret;
}

///
/// \brief Returns descriptions for all progress indicators.
/// \return List of progress indicators.
///
/// Each progess indicator will ideally result in one progress bar per progress indicator in a visual environment running the plugin module.
///
std::vector<std::string> TravellerExample::getProgressIndicators()
{
    std::vector<std::string> progressIndicators = {};
    return progressIndicators;
}

///
/// \brief Validates a JSON configuration string.
/// \param settings [in] JSON configuration string.
/// \return true if validation is OK, else returns false.
///
bool TravellerExample::validateSettings(const std::string& settings)
{
    return true;
}

///
/// \brief Aborts the image export.
///
/// This method can be called from a separate thread than the one executing processData(). It
/// will set abort flags to true, while the loops within processData() method will check these flags
/// at differnt points during execution (and exit loops if true).
///
void TravellerExample::abort()
{
    abort_ = true;
}

///
/// \brief States wether inputDataObj of processData() will be modified.
/// \return false (i.e., inputDataObj will not be modified).
///
bool TravellerExample::willInputDataObjChange()
{
    return false;
}

///
/// \brief Performs smoothing and / or downscaling on input dataset and returns a processed dataset as result.
/// \param inputDataObj [in] Instance to contain one or more series to be processed.
/// \param mapOfProcessedDataObjects [in] Not in use.
/// \param currentRoute [in] Not in use.
/// \param result [out] Contains single Series, with processed data.
/// \param addLogEntry [out] Callback function used to report log entries (in the form of strings and severity).
/// \param reportProgress [out] Callback function used to report progress (see getProgressIndicators() for more info.).
/// \param reportPartialResult [out] Not in use.
/// \return true if OK, else returns false.
///
bool TravellerExample::processData(
    PluginDataObj& /* inputDataObj */,
    const std::map<std::string, PluginDataObj>& /* mapOfProcessedDataObjects */,
    const std::string&,
    const std::string&,
    PluginDataObj& /* result */,
    std::function<void(std::string,int)>& addLogEntry,
    std::function<void(std::vector<SProgress>&)>& /* reportProgress */, 
    std::function<void(std::string,Series&)>& /* reportPartialResult */
)
{
    abort_ = false;

    // Add to log that we started
    addLogEntry("Begining data processing...", LogLevels::logStatus);
    // Do your things here
    addLogEntry("Done", LogLevels::logStatus);
    return true;
}
