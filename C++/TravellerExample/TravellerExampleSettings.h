#pragma once
#include <Utils/NNLDialogDescriptor.h>
#include <QJsonValue>
#include <vector>

class TravellerExampleSettings
{
public:
    TravellerExampleSettings(bool reinitDialogDescription=true);

public:
    std::string getID();
    std::string getDescription();
    bool validate(const std::string& settings);
    bool parse(const std::string& settings);

private:
    void resetToDefaults();
    void buildDialogDescriptor();

private:
    QString destination_;
    bool bringingFriend_;
    int numberOfFriends_;

    QString chosenVehicle_;
    QString requestedCarMake_;

    std::vector<QJsonValue> cargoList_;
    bool bringCounterfitMoney_;

    NNLDialogDescriptor dialogDescriptor_;
    QString lastSuccessfullyParsedSettings_;
};
