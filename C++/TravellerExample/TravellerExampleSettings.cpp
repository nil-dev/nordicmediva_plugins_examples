#include "TravellerExampleSettings.h"
#include <Utils/NNLDialogDescriptor/CheckBox.h>
#include <Utils/NNLDialogDescriptor/EditBoxString.h>
#include <Utils/NNLDialogDescriptor/EditBoxFloat.h>
#include <Utils/NNLDialogDescriptor/EditBoxInteger.h>
#include <Utils/NNLDialogDescriptor/DropList.h>
#include <Utils/NNLDialogDescriptor/DropListItem.h>
#include <Utils/NNLDialogDescriptor/DropListString.h>

TravellerExampleSettings::TravellerExampleSettings(bool reinitDialogDescription)
{
    resetToDefaults();
    if (reinitDialogDescription) buildDialogDescriptor();
}

void TravellerExampleSettings::resetToDefaults()
{
    destination_ = "Neverland";
    bringingFriend_ = true;
    numberOfFriends_ = 0;

    chosenVehicle_ = "car";
    requestedCarMake_ = "";

    cargoList_ = std::vector<QJsonValue>();
    bringCounterfitMoney_ = true;
}

std::string TravellerExampleSettings::getID()
{
    return "TravellerExampleSettings";
}

std::string TravellerExampleSettings::getDescription()
{
    return dialogDescriptor_.getSettingsDescription();
}

bool TravellerExampleSettings::validate(const std::string& settings)
{
    return dialogDescriptor_.validate(settings).first;
}

bool TravellerExampleSettings::parse(const std::string& settings)
{
    if(!validate(settings)) return false;
    if(!dialogDescriptor_.parse(settings)) return false;

    lastSuccessfullyParsedSettings_ = settings.c_str();

    return true;
}

void TravellerExampleSettings::buildDialogDescriptor()
{
    TravellerExampleSettings defaults(false);

    NNLDialogDescriptor settings(getID(), "Traveller example plugin", "This plugin is for showing how settings can be used");
    auto groupGeneral = settings.addGroup("General", "General", "General settings");

    //
    // Basic usage
    //
    groupGeneral->addStaticHTML(Descriptor::StaticHTML("<h1>Destination</h1><p>Where do you want to travel?</p>"));
    groupGeneral->addEditBoxString(Descriptor::EditBoxString(&destination_, "Destination", "Destination", "Imagine! Why not a planet?", defaults.destination_.toStdString()));

    NNLDialogDescriptor bringingFriendsSubOptions("BringingFriendsTrueSettings", "Friend options", "Description of friends");
    auto bringingFriendsGroup = bringingFriendsSubOptions.addGroup("General", "General", "General");
    bringingFriendsGroup->addEditBoxInteger(Descriptor::EditBoxInteger(&numberOfFriends_, "FriendCount", "How many friends?", "It's ok to be alone sometimes", defaults.numberOfFriends_));
    groupGeneral->addCheckBox(Descriptor::CheckBox(&bringingFriend_, "BringFriend", "Bring a friends", "Traveling can be fun alone, but funner with friends!", defaults.bringingFriend_, &bringingFriendsSubOptions, nullptr));
    
    
    auto dropList1 = groupGeneral->addDropList(Descriptor::DropList(&chosenVehicle_, true, "Vehicle", "Choose vehicle!", "How fast will you get there?", defaults.chosenVehicle_));
    dropList1->addItem(Descriptor::DropListString("snowmobile", "Snowmobile", "Can only drive on snow. Make sure it is snowy."));
    dropList1->addItem(Descriptor::DropListString("tardis", "Tardis", "Be aware of Daleks!"));
    
    Descriptor::DropListStringWithOptions carOption("car", "Car", "The fastest option");
    auto carOptionSubOptions = carOption.addGroup("General", "General", "General");
    carOptionSubOptions->addEditBoxString(Descriptor::EditBoxString(&requestedCarMake_, "RequestedCarMake", "Requested make of car", "We'll try to cater to your needs", defaults.requestedCarMake_.toStdString()));
    dropList1->addItem(Descriptor::DropListItem(carOption));


    // Creating a list option. Users can add or remove items from lists.
    groupGeneral->addStaticHTML(Descriptor::StaticHTML("<h1>Cargo</h1><p>Select the cargo you wish to take</p>"));
    
    auto cargoListOption = groupGeneral->addList(Descriptor::List(&cargoList_, "CargoList", "What cargo do you want to bring", "Don't bring too much! You can only bring 5 things and you must bring at least 1.", defaults.cargoList_, true, 1, 5));
    cargoListOption->addItem(Descriptor::DropListItem(Descriptor::DropListString("apples", "Apples", "Bing apples! They're tasty!")));
    cargoListOption->addItem(Descriptor::DropListItem(Descriptor::DropListString("bananas", "Bananas", "Some people like them!")));

    Descriptor::DropListStringWithOptions moneyCargoOption("money", "Money", "Be careful!");
    auto moneyCargoSubOptions = moneyCargoOption.addGroup("General", "General", "General");
    moneyCargoSubOptions->addCheckBox(Descriptor::CheckBox(&bringCounterfitMoney_, "CounterfitMoney", "Bring counterfit money?", "It can make you rich, but is very risky.", defaults.bringCounterfitMoney_, nullptr, nullptr));
    cargoListOption->addItem(Descriptor::DropListItem(moneyCargoOption));


    dialogDescriptor_ = settings;
}
