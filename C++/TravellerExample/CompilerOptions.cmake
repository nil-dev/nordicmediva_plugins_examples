# compiler options
if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    message(STATUS "Using Clang")

    # Wall enables most warnings. Wextra enables several extra warnings.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")

    # overloaded-virtual gives warnings when a function hides a function of the same name from an inherited class.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Woverloaded-virtual")

    # conversion enables warnings on possible loss of data during conversions.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wconversion")

    # old-style-cast complains about C-style casting.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wold-style-cast")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wcast-qual")

    # write-strings gives warnings about deprecated string to char* conversions
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wwrite-strings")

    # unreachable-code
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wunreachable-code")

    # Warn when the compiler detects paths that dereferences a null pointer.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wnull-dereference")

    # Added security related warnings for printf and the like.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wformat=2")

    # Causes warnings in Qt. disabled.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-gnu-zero-variadic-macro-arguments")

    if (CMAKE_BUILD_TYPE STREQUAL "Debug")
        # Turn warnings into errors
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror")
    endif()

    # Set pedantry to maximum :)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic")

elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    message(STATUS "Using GCC")

    # Wall enables most warnings. Wextra enables several extra warnings.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")

    # overloaded-virtual gives warnings when a function hides a function of the same name from an inherited class.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Woverloaded-virtual")

    # conversion enables warnings on possible loss of data during conversions.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wconversion")

    # old-style-cast complains about C-style casting.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wold-style-cast")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wcast-qual")

    # suggest-override gives warnings when override should be used
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wsuggest-override")

    # write-strings gives warnings about deprecated string to char* conversions
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wwrite-strings")

    # unreachable-code
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wunreachable-code")

    # Warn about duplicated condition in if-else-if chains
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wduplicated-cond")

    # Warn when the compiler detects paths that dereferences a null pointer.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wnull-dereference")

    # Added security related warnings for printf and the like.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wformat=2")

    # logical-op gives warnings on redundant logical operations
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wlogical-op")

    # For newer versions of gcc
    if(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER 7.0)
        # shadow=compatible-local is only available on newer gcc versions
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wshadow=compatible-local")

        # Warn when an if-else has identical branches.
        # It also warns for conditional operators having identical second and third expressions.
        #set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wduplicated-branches")
    endif()

    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        # Turn warnings into errors
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror")
    endif()

    # Set pedantry to maximum :)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ansi -pedantic")

	# Turn off multi line comment warning
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-comment")


elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
    message(STATUS "Using MSVC")
    # /GR /EHsc are enabled by default. (using MSVC2017.3. MS may change this).
    # EHsc enables normal C++ exceptions.
    # GR enables run-time type information. (used by type-id and dynamic_cast<>).

    # nologo suppresses the display of the copyright banner.
    # Gy enables function level linking. May reduce binary size.
    # permissive- sets strict mode. Non-standard c++ should not be excepted.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /nologo /Gy /permissive-")

    # Warning level 4 is the highest except for Wall.
    STRING(REGEX REPLACE "/W[0-9]" "/W4" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})
    # Ignores the following warnings due to failures in third party includes:
    # (C4127)conditionals that are constant (Qt).
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4127")

    if(CMAKE_BUILD_TYPE STREQUAL "Debug")
        # /WX turns warnings to errors.
        # Do not use together with analyze as this will
        # stop the analysis on the first warning.
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /WX")

		# A deprecation warning that happens when passing Eigen::SVDBase as function argument.
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D_SCL_SECURE_NO_WARNINGS")
    endif()

    # sdl adds recommended Security Development Lifecycle (SDL) checks.
    # Also enables some runtime checks. This may affect performance.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /sdl") 

    # added Order of Members Initialization Warning.
    # this will be added to the default warnings level
    # by microsoft at a later date.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /w15038")

    # run MSVC static analyzer
	#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /analyze")

	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D_SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING")

endif()
message(STATUS "Compiler flags are ${CMAKE_CXX_FLAGS}")

