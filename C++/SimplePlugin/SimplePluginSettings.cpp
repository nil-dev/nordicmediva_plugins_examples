#include "SimplePluginSettings.h"
#include <Utils/NNLDialogDescriptor/CheckBox.h>
#include <Utils/NNLDialogDescriptor/EditBoxString.h>
#include <Utils/NNLDialogDescriptor/EditBoxFloat.h>
#include <Utils/NNLDialogDescriptor/EditBoxInteger.h>

SimplePluginSettings::SimplePluginSettings(bool reinitDialogDescription)
{
    resetToDefaults();
    if (reinitDialogDescription) buildDialogDescriptor();
}

void SimplePluginSettings::resetToDefaults()
{
    myBooleanOption_ = true;
    myStringOption_ = "Hello world";
    myIntOption_ = 42;
    myFloatOption_ = 3.14;
}

std::string SimplePluginSettings::getID()
{
    return "SimplePlugin";
}

std::string SimplePluginSettings::getDescription()
{
    return dialogDescriptor_.getSettingsDescription();
}

bool SimplePluginSettings::validate(const std::string& settings)
{
    return dialogDescriptor_.validate(settings).first;
}

bool SimplePluginSettings::parse(const std::string& settings)
{
    if(!validate(settings)) return false;
    if(!dialogDescriptor_.parse(settings)) return false;

    lastSuccessfullyParsedSettings_ = settings.c_str();

    return true;
}

void SimplePluginSettings::buildDialogDescriptor()
{
    SimplePluginSettings defaults(false);

    NNLDialogDescriptor settings(getID(), "Simple plugin", "This plugin is simple");
    auto groupGeneral = settings.addGroup("General", "General", "General settings");

    groupGeneral->addCheckBox(Descriptor::CheckBox(&myBooleanOption_, "MyBoolean", "My boolean option", "Toggle this on and off", defaults.myBooleanOption_, nullptr, nullptr));
    groupGeneral->addEditBoxFloat(Descriptor::EditBoxFloat(&myFloatOption_, "MyFloat", "My float option", "I am floating!", defaults.myFloatOption_));
    groupGeneral->addEditBoxInteger(Descriptor::EditBoxInteger(&myIntOption_, "MyIntOption", "My int option", "I am an int option", defaults.myIntOption_));
    groupGeneral->addEditBoxString(Descriptor::EditBoxString(&myStringOption_, "MyStringOption", "My string option", "Feels stringy", defaults.myStringOption_.toStdString()));

    dialogDescriptor_ = settings;
}
