#pragma once
#include <Utils/NNLDialogDescriptor.h>

class SimplePluginSettings
{
public:
    SimplePluginSettings(bool reinitDialogDescription=true);

public:
    std::string getID();
    std::string getDescription();
    bool validate(const std::string& settings);
    bool parse(const std::string& settings);

private:
    void resetToDefaults();
    void buildDialogDescriptor();

private:
    bool myBooleanOption_;
    QString myStringOption_;
    int myIntOption_;
    float myFloatOption_;

    NNLDialogDescriptor dialogDescriptor_;
    QString lastSuccessfullyParsedSettings_;
};
