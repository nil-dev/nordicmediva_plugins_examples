#pragma once
#include <PluginInterface/PluginInterface.h>
#include <vector>
#include <memory>
#include <string>

#include "SimplePluginSettings.h"

class SimplePlugin : public PluginInterface
{
public:
    SimplePlugin();
    virtual ~SimplePlugin() override;

public:
    virtual SInterfaceID getInterfaceID() override;
    virtual SSettingsDescription getSettingsDescription() override;
    virtual std::vector<std::string> getProgressIndicators() override;
    virtual bool validateSettings(const std::string& settings) override;
    void abort() override;
    virtual bool willInputDataObjChange() override;

    virtual bool processData(PluginDataObj& inputDataObj, const std::map<std::string, PluginDataObj>& mapOfProcessedDataObjects,
                             const std::string& currentRouteSettings, const std::string& currentQueueSettings, PluginDataObj& result,
                             std::function<void(std::string,int)>& addLogEntry,
                             std::function<void(std::vector<SProgress>&)>& reportProgress,
                             std::function<void(std::string,Series&)>& reportPartialResult) override;

private:
    bool abort_;
    SimplePluginSettings settings_;
};


extern "C" BOOST_SYMBOL_EXPORT PluginFactory<SimplePlugin> matt_plugin;
PluginFactory<SimplePlugin> matt_plugin;
