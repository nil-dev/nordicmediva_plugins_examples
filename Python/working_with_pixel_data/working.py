import sys
import medivapylib as mpl
import numpy as np

# These four parameters will be passed by the python interpreter plugin, but can also be passed manually outside the plugin
dicomIn = sys.argv[1]
dicomOut = sys.argv[2]
dicomInPreproc = sys.argv[3] if len(sys.argv) > 3 else ""
instanceUUID = sys.argv[4] if len(sys.argv) > 4 else ""

# Get the input
dataIn = mpl.Input(dicomIn, dicomInPreproc, instanceUUID)
inputSeries = dataIn[0]

###########################################################
#                  Creating a new series                  #
###########################################################
# Create a with same dimensions as input series.
# * Relevant DICOM headers are copied
# * All voxels are zero-valued
outputSeries1 = mpl.SeriesCreator().createEmptySeries(inputSeries)
outputSeries2 = mpl.SeriesCreator().createEmptySeries(inputSeries)

##########################################################
#                 Setting the pixel data                 #
##########################################################
# Method 1:
numVolumes = inputSeries.getVolumeCount()
numSlices = inputSeries.getSlicesPerVolumeCount()
numRows = inputSeries.getRowCount()
numColumns = inputSeries.getColumnCount()
for v in range(numVolumes):
    volumeObj = outputSeries1[v] # Get the Volume object at index v
    for s in range(numSlices):
        sliceObj = volumeObj[s] # Get the Slice object at index s
        for r in range(numRows):
            rowObj = sliceObj[r]
            for c in range(numColumns):
                rowObj[c] = np.random.random()
                # NOTE! The above is equivalent to saying
                # outputSeries1[v][s][r][c] = np.random.random()
                # but by extracting each object yields much faster
                # processing times
        
# Method 2:
for v in range(numVolumes):
    for s in range(numSlices):
        # NOTE! The pixelData array needs to be flat!
        pixelData = np.random.random((numRows, numColumns)).flatten()
        outputSeries2[v][s].setPixelData(pixelData)

dataOut = mpl.Output(dicomOut, instanceUUID)
dataOut.add(outputSeries1, "OutoutSeries1")
dataOut.add(outputSeries2, "OutoutSeries2")
