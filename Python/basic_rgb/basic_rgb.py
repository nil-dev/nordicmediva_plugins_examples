import sys
import medivapylib as mpl
import numpy as np

# These four parameters will be passed by the python interpreter plugin, but can also be passed manually outside the plugin
dicomIn = sys.argv[1]
dicomOut = sys.argv[2]
dicomInPreproc = sys.argv[3] if len(sys.argv) > 3 else ""
instanceUUID = sys.argv[4] if len(sys.argv) > 4 else ""

# Get the input
dataIn = mpl.Input(dicomIn, dicomInPreproc, instanceUUID)
inputSeries = dataIn[0]

###########################################################
#                  Creating a new series                  #
###########################################################
# Create a with same dimensions as input series.
# * Relevant DICOM headers are copied
# * All voxels are zero-valued
outputSeries = mpl.SeriesCreator().createEmptySeries(inputSeries)

# Mark the series as an RGB series
outputSeries.setRGB(mpl.RGB.IsRGB)

##########################################################
#                 Setting the pixel data                 #
##########################################################
numVolumes = outputSeries.getVolumeCount()
numSlices = outputSeries.getSlicesPerVolumeCount()
numRows = outputSeries.getRowCount()
numColumns = outputSeries.getColumnCount()
for v in range(numVolumes):
    for s in range(numSlices):
        # Generate a slice where the first 1/3 rows are painted red,
        # the middle 1/3 rows are painted green, and the remaining
        # 1/3 rows are painted blue.
        redPixelData = np.zeros((numRows, numColumns))
        redPixelData[:int(numRows/3),:] = 1
        outputSeries[v][s].setRedPixelData(redPixelData.flatten())

        greenPixelData = np.zeros((numRows, numColumns))
        greenPixelData[int(numRows/3):int(2*numRows/3),:] = 1
        outputSeries[v][s].setGreenPixelData(greenPixelData.flatten())

        bluePixelData = np.zeros((numRows, numColumns))
        bluePixelData[int(2*numRows/3):,:] = 1
        outputSeries[v][s].setBluePixelData(bluePixelData.flatten())

dataOut = mpl.Output(dicomOut, instanceUUID)
dataOut.add(outputSeries, "RGB Series")
