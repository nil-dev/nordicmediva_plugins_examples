# nordicMEDiVA Python examples

This repo contains some examples to create Python scripts to be run within the nordicMEDiVA environment. Using these examples, you have an entrypoint to run python scripts locally for debugging purposes. The python scripts here can be added as extensions to the Python Interpreter plugin in nordicMEDiVA with no modifications.

## Input Data
The folder structure for the input-data is

./in/series_1/filename1.dcm
./in/series_1/filename2.dcm

./in/series_2/filename1.dcm
./in/series_2/filename2.dcm

etc. I.e., each series is located in it's own sub-folder inside the input folder.

**You must provide your own DICOM data in these folders**

## Running the `Invert` example
First, add data in the input folder. Only the one of the folders are considered in the analysis.

### Running the script
Run the regular python script with the following command
```
$> python3 ./invert/invert.py ./invert/in ./invert/out
```

### Running the iPython notebook
**Assuming you are using Visual Stuido Code**

Simply open the notebook and click the "Run All Cells" button. If you get a notification in VSCode asking you to trust the notebook, agree to do so by clicking the "Trust" button.
