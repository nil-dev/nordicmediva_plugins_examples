import sys
import medivapylib as mpl
import numpy as np
from invert_utils import find_max_in_series, invert_data

# These four parameters will be passed by the python interpreter plugin, but can also be passed manually outside the plugin
dicomIn = sys.argv[1]
dicomOut = sys.argv[2]
dicomInPreproc = sys.argv[3] if len(sys.argv) > 3 else ""
instanceUUID = sys.argv[4] if len(sys.argv) > 4 else ""

# Get the input
dataIn = mpl.Input(dicomIn, dicomInPreproc, instanceUUID)
series = dataIn[0]

# Invert all voxels (i.e., find maximum, m and calclate m-x for each voxel value, x)
outputSeries = invert_data(series)

# Set the output
dataOut = mpl.Output(dicomOut, instanceUUID)
dataOut.add(outputSeries, "MyInvertedOutput")
