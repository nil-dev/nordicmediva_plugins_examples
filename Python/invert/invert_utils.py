import medivapylib as mpl
import numpy as np

def find_max_in_series(inputSeries):
    """
        Returns the maximum voxel value in the series.
    """
    volumeCount = inputSeries.getVolumeCount()
    sliceCount = inputSeries.getSlicesPerVolumeCount()

    inputSeries[0][0].getPixelData()
    maxVal = 0

    for v in range(volumeCount):
        print("Max calc (%i / %i)" % (v, volumeCount))
        for s in range(sliceCount):
            currMax = np.max(inputSeries[v][s].getPixelData())
            if currMax > maxVal:
                maxVal = currMax
    return maxVal

def invert_data(inputSeries):
    """
        Inverts data. A new Series object is created and each voxel is set to
        max(inputSeries) - inputSeries.
    """
    volumeCount = inputSeries.getVolumeCount()
    sliceCount = inputSeries.getSlicesPerVolumeCount()

    outputSeries = mpl.SeriesCreator().createEmptySeries(inputSeries)

    maxVal = find_max_in_series(inputSeries)

    for v in range(volumeCount):
        print("Invert calc (%i / %i)" % (v, volumeCount))
        for s in range(0, sliceCount):
            originalPixelData = inputSeries[v][s].getPixelData()
            converted = maxVal - originalPixelData
            outputSeries[v][s].setPixelData(converted)

    return outputSeries
